# SocketIO Nodejs Chat - LiveCoding Twitch

This is a node.js chat application Live coded on twitch and powered by Socket.io and nodejs built-in server that provides the basic functions you'd expect from a chat, such as send private messages, subscribe a room, middleware rejection, an admin system, etc.

![Chat Preview](docs/screenshot.png)

---

## Features

- Tailwind CSS
- Private Channel
- Real Time Chat
- Private messaging

---

## Setup

Clone this repo to your desktop and run `npm install` to install all the dependencies.
then you can run `npm run dev` to start the project and you are good to go.

---

## Usage

After you clone this repo to your desktop, go to its root directory and run `npm install` to install its dependencies.

Once the dependencies are installed, you can run `npm run dev` to start the application. You will then be able to access it at localhost:3000

---

## License

> You can check out the full license [here](https://github.com/IgorAntun/node-chat/blob/master/LICENSE)

This project is licensed under the terms of the **MIT** license.
