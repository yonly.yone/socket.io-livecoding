import { createServer } from "http";
import { Server } from "socket.io";
import { instrument } from "@socket.io/admin-ui";

const httpServer = createServer();

const io = new Server(httpServer, {
  cors: {
    credentials: true,
    origin: ["http://localhost:5173", "https://admin.socket.io"],
  },
});

const userIO = io.of("/user");

userIO.use((socket, next) => {
  const userName = socket.handshake.auth.userName;
  const connectedUsersList = [...userIO.sockets.values()].map(
    (el) => el.handshake.auth.userName,
  );
  socket.userName = userName;

  if (connectedUsersList.find((el) => el === userName)) {
    next(new Error(`Le nom d'utilisateur ${userName} est déja pris!`));
  }
  next();
});

userIO.on("connection", (socket) => {
  socket.on("message", (event, room) => {
    console.log({ event, room });
    if (room && room !== "") {
      socket.to(room).emit("message", event);
    } else {
      socket.broadcast.emit("message", event);
    }
  });

  socket.on("join", (room, cb) => {
    const joinResult = socket.join(room);
    cb("Vous avez bien rejoins la room : " + room);
  });
});

instrument(io, {
  mode: "development",
  namespaceName: "/admin",
  auth: false,
});

httpServer.listen(3000);
