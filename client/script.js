import { io } from "socket.io-client";

let socket = null;

const localStorageToken = "UserCreds",
  parser = new DOMParser(),
  format = "text/html";

const getUserValue = () => JSON.parse(localStorage.getItem(localStorageToken));
const setUserValue = (value) =>
  localStorage.setItem(localStorageToken, JSON.stringify(value));
const isUserValueDefined = () => getUserValue() !== null;

const settingButton = document.querySelector("button#settings-button");
const chatform = document.querySelector("form#chat-form");
const userNameForm = document.querySelector("form#userName");
const roomForm = document.querySelector("form#room");
const chatbox = document.querySelector("#chatbox");
const userNameInput = document.querySelector("input#userName");
const messageInput = document.querySelector("input#message");
const roomInput = document.querySelector("input#room");
const modal = document.querySelector("#modal");
const modalOverlay = document.querySelector("#modal-overlay");
const connectionState = document.querySelector("#connection-state");

const connectionStateClass = {
  success: "bg-green-400",
  error: "bg-red-400",
};

const handleConnectState = () => {
  connectionState.classList.add(connectionStateClass.success);
  connectionState.classList.remove(connectionStateClass.error);
};

const handleDisconnectState = () => {
  connectionState.classList.remove(connectionStateClass.success);
  connectionState.classList.add(connectionStateClass.error);
};
const getSocket = () => {
  const socket = io("http://localhost:3000/user", {
    auth: { userName: getUserValue().userName },
  });

  socket.on("connect", () => {
    handleConnectState();
    renderAdminMessage(
      `Bonjour ${
        getUserValue().userName
      } vous êtes bien connecté. votre salon privé est le: ${socket.id}`,
    );
  });

  socket.on("message", ({ userName, message }) => {
    chatbox.appendChild(renderSenderChat(userName, message));
  });

  socket.on("connect_error", (error) => {
    handleDisconnectState();
    renderAdminMessage(error);
  });

  socket.on("disconnect", () => {
    handleDisconnectState();
    renderAdminMessage("Déconnecté!");
  });

  return socket;
};

const renderUserChat = (message) => {
  return parser.parseFromString(
    `
      <div class="flex items-end justify-end">
          <div class="bg-blue-500 p-3 rounded-lg">
            <p class="text-sm text-white">${message}</p>
          </div>
          <img
            src="https://pbs.twimg.com/profile_images/1707101905111990272/Z66vixO-_normal.jpg"
            alt="User Avatar"
            class="w-8 h-8 rounded-full ml-3"
          />
      </div> 
  `,
    format,
  ).body.firstChild;
};

const renderSenderChat = (username, message) => {
  return parser.parseFromString(
    `
    <div class="flex items-start">
            <img
              src="https://pbs.twimg.com/profile_images/1707101905111990272/Z66vixO-_normal.jpg"
              alt="Other User Avatar"
              class="w-8 h-8 rounded-full ml-3"
            />
            <div class="ml-3 bg-gray-100 p-3 rounded-lg">
              <p class="text-sm text-bold">${username} :</p>
              <p class="text-sm text-gray-800">
                ${message}
              </p>
            </div>
    </div>
  `,
    format,
  ).body.firstChild;
};

const renderAdminMessage = (message) =>
  chatbox.appendChild(renderSenderChat("admin", message));

const showModal = () => {
  if (isUserValueDefined()) {
    const { userName, room } = getUserValue();
    userNameInput.value = userName ?? "";
    roomInput.value = room ?? "";
  }

  modal.classList.remove("hidden");
  modalOverlay.classList.remove("hidden");
};

const hideModal = () => {
  modal.classList.add("hidden");
  modalOverlay.classList.add("hidden");
};

const handleChatForm = (event) => {
  event.preventDefault();
  const message = messageInput.value;

  if (message === "") {
    return;
  }

  const { userName, room } = getUserValue();
  chatbox.appendChild(renderUserChat(message));
  socket.emit("message", { userName, message }, room);
  messageInput.value = "";
};

const handleChangeUserName = (event) => {
  event.preventDefault();
  const userName = userNameInput.value;
  const room = roomInput.value;

  if (userName === "") return;

  if (socket) socket.disconnect();

  setUserValue({ userName, room });
  hideModal();
  socket = getSocket();
};

const handleJoinRoom = (event) => {
  event.preventDefault();

  const userName = userNameInput.value;
  const room = roomInput.value;

  if (room == "") return;

  setUserValue({ userName, room });
  socket.emit("join", room, (message) => renderAdminMessage(message));
  hideModal();
};

const handleToogleConnection = () => {
  if (socket) {
    socket.disconnect();
    socket = null;
    return;
  }
  socket = getSocket();
};

window.onload = function () {
  if (isUserValueDefined()) {
    socket = getSocket();
  } else {
    showModal();
  }

  settingButton.addEventListener("click", showModal);
  userNameForm.addEventListener("submit", handleChangeUserName);
  roomForm.addEventListener("submit", handleJoinRoom);
  chatform.addEventListener("submit", handleChatForm);
  connectionState.addEventListener("click", handleToogleConnection);
};
